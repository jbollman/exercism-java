public class ReverseString {

    public String reverse(String inputString) {
        if (inputString == "") {
            return inputString;
        }
        else {
            StringBuffer sb = new StringBuffer(inputString);
            sb = sb.reverse();
            inputString = sb.toString();
            return inputString;
        }
    }
  
}

// i can do a for loop / split to generate an array of chars;
// with an array i can do another for loop and always grab the last element in the array [-1]
// concatenate the array back into a string.